package app.pott.barista.database.model.ticket;

public enum TicketStatus {
	REJECTED, UNRESOLVED, ACCEPTED 
}

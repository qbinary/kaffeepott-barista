package app.pott.barista.database.model.ticket;

import org.json.JSONObject;

import app.pott.barista.Constants.JsonC;

public class TicketArgumentValidator {

	private String[] selectKeys;
	private String[] changeKeys;

	public TicketArgumentValidator(String[] selectKeys, String[] changeKeys) {
		this.selectKeys = selectKeys;
		this.changeKeys = changeKeys;
	}

	public boolean isValid(JSONObject args) {
		JSONObject select = args.getJSONObject(JsonC.SELECT);
		JSONObject change = args.getJSONObject(JsonC.CHANGE);

		// Check length
		if (select.length() != selectKeys.length || change.length() != changeKeys.length)
			return false;

		//// Check keys
		// select
		for (final String s : selectKeys)
			if (!select.has(s))
				return false;

		// change
		for (final String s : changeKeys)
			if (!change.has(s))
				return false;

		return true;
	}
}

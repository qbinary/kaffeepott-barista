package app.pott.barista.database.model.helper;

import java.util.List;

import app.pott.barista.database.model.entities.OpeningRange;
import app.pott.barista.database.model.entities.Ticket;
import app.pott.barista.database.model.product.ProductSize;
import app.pott.barista.database.model.ticket.CompanionType;
import app.pott.barista.database.model.ticket.TicketStatus;

public class TicketBuilder {

	// Required
	private String userId = null;

	private Long affectedCafeId = null;
	private String affectedProductName = null;
	private ProductSize affectedProductSize = null;
	private String newBadgeType = null;
	private Double newLocationLon = null;
	private Double newLocationLat = null;
	private String newCafeName = null;
	private Double newProductPrice = null;
	private CompanionType companion = null;
	private TicketStatus status = null;
	private List<OpeningRange> newOpeningRanges = null;

	public TicketBuilder(String userId) {
		this.userId = userId;
	}

	public void setAffectedCafeId(Long affectedCafeId) {
		this.affectedCafeId = affectedCafeId;
	}

	public String getAffectedProductName() {
		return affectedProductName;
	}

	public void setAffectedProductName(String affectedProductName) {
		this.affectedProductName = affectedProductName;
	}

	public ProductSize getAffectedProductSize() {
		return affectedProductSize;
	}

	public void setAffectedProductSize(ProductSize affectedProductSize) {
		this.affectedProductSize = affectedProductSize;
	}

	public String getNewBadgeType() {
		return newBadgeType;
	}

	public void setNewBadgeType(String newBadgeType) {
		this.newBadgeType = newBadgeType;
	}

	public double getNewLocationLon() {
		return newLocationLon;
	}

	public void setNewLocationLon(double newLocationLon) {
		this.newLocationLon = newLocationLon;
	}

	public double getNewLocationLat() {
		return newLocationLat;
	}

	public void setNewLocationLat(double newLocationLat) {
		this.newLocationLat = newLocationLat;
	}

	public String getNewCafeName() {
		return newCafeName;
	}

	public void setNewCafeName(String newCafeName) {
		this.newCafeName = newCafeName;
	}

	public double getNewProductPrice() {
		return newProductPrice;
	}

	public void setNewProductPrice(double newProductPrice) {
		this.newProductPrice = newProductPrice;
	}

	public CompanionType getCompanion() {
		return companion;
	}

	public void setCompanion(CompanionType companion) {
		this.companion = companion;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public List<OpeningRange> getNewOpeningRanges() {
		return newOpeningRanges;
	}

	public void setNewOpeningRanges(List<OpeningRange> newOpeningRanges) {
		this.newOpeningRanges = newOpeningRanges;
	}

	public String getUserId() {
		return userId;
	}

	public long getAffectedCafeId() {
		return affectedCafeId;
	}

	public Ticket build() {
		return new Ticket(userId, affectedCafeId, affectedProductName, affectedProductSize, newBadgeType,
				newLocationLon, newLocationLat, newCafeName, newProductPrice, companion, status, newOpeningRanges);
	}
}

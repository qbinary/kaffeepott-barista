package app.pott.barista.database.model.entities;

public class CafeRating {

	private long cafeId;
	private String username;
	private int rating;

	public CafeRating(long cafeId, String username, int rating) {
		this.cafeId = cafeId;
		this.username = username;
		this.rating = rating;
	}

	public long getCafeId() {
		return cafeId;
	}

	public int getRating() {
		return rating;
	}

	public String getUsername() {
		return username;
	}

}

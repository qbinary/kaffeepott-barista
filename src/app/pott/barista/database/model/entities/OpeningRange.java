package app.pott.barista.database.model.entities;

import java.sql.Timestamp;

public class OpeningRange {
	private String day;
	private Timestamp openingTime;
	private Timestamp closingTime;

	public OpeningRange(String day, Timestamp openingTime, Timestamp closingTime) {
		this.day = day;
		this.openingTime = openingTime;
		this.closingTime = closingTime;
	}

	public Timestamp getClosingTime() {
		return closingTime;
	}

	public String getDay() {
		return day;
	}

	public Timestamp getOpeningTime() {
		return openingTime;
	}
}

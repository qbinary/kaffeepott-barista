package app.pott.barista.database.model.entities;

public class CafeProduct {

	private long cafeId;
	private String productTypeName;
	private Size size;
	private double price;

	public CafeProduct(long cafeId, String productTypeName, Size size, double price) {
		this.cafeId = cafeId;
		this.productTypeName = productTypeName;
		this.size = size;
		this.price = price;
	}

	public long getCafeId() {
		return cafeId;
	}

	public double getPrice() {
		return price;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public Size getSize() {
		return size;
	}
}

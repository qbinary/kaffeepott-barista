package app.pott.barista.database.model.entities;

import java.util.List;

import app.pott.barista.database.model.product.ProductSize;
import app.pott.barista.database.model.ticket.CompanionType;
import app.pott.barista.database.model.ticket.TicketStatus;

public class Ticket {
	private String userId;
	private Long affectedCafeId;
	private String affectedProductName;
	private ProductSize affectedProductSize;
	private String newBadgeType;
	private Double newLocationLon;
	private Double newLocationLat;
	private String newCafeName;
	private Double newProductPrice;
	private CompanionType companion;
	private TicketStatus status;
	private List<OpeningRange> newOpeningRanges;

	public Ticket(String userId, Long affectedCafeId, String affectedProductName, ProductSize affectedProductSize,
			String newBadgeType, Double newLocationLon, Double newLocationLat, String newCafeName,
			Double newProductPrice, CompanionType companion, TicketStatus status, List<OpeningRange> newOpeningRanges) {
		this.userId = userId;
		this.affectedCafeId = affectedCafeId;
		this.affectedProductName = affectedProductName;
		this.affectedProductSize = affectedProductSize;
		this.newBadgeType = newBadgeType;
		this.newLocationLon = newLocationLon;
		this.newLocationLat = newLocationLat;
		this.newCafeName = newCafeName;
		this.newProductPrice = newProductPrice;
		this.companion = companion;
		this.status = status;
		this.newOpeningRanges = newOpeningRanges;
	}

	public String getUserId() {
		return userId;
	}

	public Long getAffectedCafeId() {
		return affectedCafeId;
	}

	public String getAffectedProductName() {
		return affectedProductName;
	}

	public ProductSize getAffectedProductSize() {
		return affectedProductSize;
	}

	public String getNewBadgeType() {
		return newBadgeType;
	}

	public Double getNewLocationLon() {
		return newLocationLon;
	}

	public Double getNewLocationLat() {
		return newLocationLat;
	}

	public String getNewCafeName() {
		return newCafeName;
	}

	public Double getNewProductPrice() {
		return newProductPrice;
	}

	public Boolean isHasOpeningChange() {
		return newOpeningRanges != null && newOpeningRanges.size() > 0;
	}

	public CompanionType getCompanion() {
		return companion;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public List<OpeningRange> getNewOpeningRanges() {
		return newOpeningRanges;
	}
}

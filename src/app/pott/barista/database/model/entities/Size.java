package app.pott.barista.database.model.entities;

public enum Size {
	XS, S, M, L, XL
}

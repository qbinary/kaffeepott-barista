package app.pott.barista.database.model.entities;

public class Cafe {

	private String name;
	private long id;
	private double longitude;
	private double latitude;

	public Cafe(String name, long id, double longitude, double latitude) {
		this.name = name;
		this.id = id;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public long getId() {
		return id;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getName() {
		return name;
	}
}

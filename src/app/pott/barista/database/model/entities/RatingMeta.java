package app.pott.barista.database.model.entities;

import java.util.LinkedList;
import java.util.List;

public class RatingMeta {
	private LinkedList<CafeRating> ratings;
	
	private int ratingCount;
	private double avg;

	public RatingMeta(List<CafeRating> ratings) {
		this.ratings = new LinkedList<>(ratings);
		this.ratingCount = ratings.size();
		
		//// CALCULATE AVERAGE
		// Sum up all ratings
		for (final CafeRating r : ratings) {
			avg += r.getRating();
		}

		// Divide
		if (ratings.size() != 0)
			avg /= ratings.size();
	}

	public double getAvg() {
		return avg;
	}

	public int getRatingCount() {
		return ratingCount;
	}
	
	@SuppressWarnings("unchecked")
	public LinkedList<CafeRating> getRatings() {
		return (LinkedList<CafeRating>) ratings.clone();
	}
}

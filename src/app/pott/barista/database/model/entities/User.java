package app.pott.barista.database.model.entities;

public class User {
	private String userId;
	private String ip;

	public User(String userId, String ip) {
		this.userId = userId;
		this.ip = ip;
	}

	public String getIp() {
		return ip;
	}

	public String getUserId() {
		return userId;
	}

}

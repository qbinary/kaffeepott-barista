package app.pott.barista.database.model.product;

public enum ProductSize {
	XS, S, M, L, XL
}

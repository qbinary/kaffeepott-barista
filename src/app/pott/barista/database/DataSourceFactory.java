package app.pott.barista.database;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import app.pott.barista.Constants.DataSourceC;
import app.pott.barista.Constants.PlaceholderC;
import app.pott.barista.log.Log;

public class DataSourceFactory {

	public static DataSource newMariaDataSource(String ip, String port, String dbName, String user, String password) {
		PoolProperties p = new PoolProperties();

		// Build URL
		String url = DataSourceC.DB_URL.replace(PlaceholderC.IP, ip)
				.replace(PlaceholderC.PORT, port).replace(PlaceholderC.DB, dbName);
		Log.d("Instantiating mariadb datasource with url '" + url + "'", DataSourceFactory.class);

		// Set URL and driver
		p.setUrl(url);
		p.setDriverClassName("org.mariadb.jdbc.Driver");

		// Set credentials
		p.setUsername(user);
		p.setPassword(password);

		// Misc
		p.setJmxEnabled(true);
		p.setTestWhileIdle(false);
		p.setTestOnBorrow(true);
		p.setValidationQuery("SELECT 1");
		p.setTestOnReturn(false);
		p.setValidationInterval(30000);
		p.setTimeBetweenEvictionRunsMillis(30000);
		p.setInitialSize(10);
		p.setMaxWait(10000);
		p.setRemoveAbandonedTimeout(60);
		p.setMinEvictableIdleTimeMillis(30000);
		p.setLogAbandoned(true);
		p.setRemoveAbandoned(true);
		p.setRemoveAbandonedTimeout(10000);
		p.setMaxActive(100);
		p.setMinIdle(10);

		return new DataSource(p);
	}
}

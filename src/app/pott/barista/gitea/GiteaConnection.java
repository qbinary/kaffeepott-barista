package app.pott.barista.gitea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import app.pott.barista.Constants.GiteaC;
import app.pott.barista.Constants.HttpC;

/**
 * Class to hold connection information and execute API calls on the Gitea API.
 * 
 * @author epileptic
 */
public class GiteaConnection {

	private URL url;
	private String token;

	public GiteaConnection(URL url, String token) {
		this.url = url;
		this.token = token;
	}

	/**
	 * Does a HTTP GET request on the Gitea API.
	 * 
	 * @param apiPath URL to GET from
	 * @return {@link String} apiPath
	 * @throws IOException
	 */
	public String get(String apiPath) throws IOException {
		// Open connection
		URL actual = new URL(url.toString() + apiPath);
		HttpsURLConnection https = (HttpsURLConnection) actual.openConnection();

		https.setRequestMethod(HttpC.GET_METHOD);

		// Setup response reader
		StringBuilder responseBuilder = new StringBuilder();
		try (BufferedReader responseReader = new BufferedReader(new InputStreamReader(https.getInputStream()))) {
			// Read response
			while (responseReader.ready())
				responseBuilder.append(responseReader.readLine());

		}
		return responseBuilder.toString();
	}

	/**
	 * Does a HTTP POST request on the Gitea API.
	 * 
	 * @param apiPath URL to POST to
	 * @param content 
	 * @return
	 * @throws IOException
	 */
	public String post(String apiPath, String content) throws IOException {
		// Open connection
		URL actual = new URL(url.toString() + apiPath);
		HttpsURLConnection https = (HttpsURLConnection) actual.openConnection();

		// Setup connection
		https.setDoOutput(true);
		https.setRequestMethod(HttpC.POST_METHOD);

		// Set communication mimetypes
		https.setRequestProperty(GiteaC.ACCEPT_HEADER_CONTENT, GiteaC.ACCEPT_HEADER_NAME);
		https.setRequestProperty(GiteaC.CONTENT_TYPE_HEADER_NAME, GiteaC.CONTENT_TYPE_HEADER_CONTENT);

		// Set auth token
		https.setRequestProperty(GiteaC.AUTHORIZATION_HEADER_NAME, GiteaC.AUTHORIZATION_HEADER_CONTENT.replace(token));

		// Write content
		try (OutputStreamWriter contentWriter = new OutputStreamWriter(https.getOutputStream())) {
			contentWriter.write(content);
			contentWriter.flush();
		}

		// Setup response reader
		StringBuilder responseBuilder = new StringBuilder();
		try (BufferedReader responseReader = new BufferedReader(new InputStreamReader(https.getInputStream()))) {
			// Read response
			while (responseReader.ready())
				responseBuilder.append(responseReader.readLine());
		}
		return responseBuilder.toString();
	}
}

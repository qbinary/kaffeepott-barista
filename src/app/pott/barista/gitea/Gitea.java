package app.pott.barista.gitea;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.pott.barista.Constants.GiteaC;
import app.pott.barista.Constants.JsonC;

/**
 * Gitea API wrapper
 * 
 * @author epileptic
 */
public class Gitea {

	private GiteaConnection connection;

	/**
	 * @param server URL to the gitea API of your instance e.g.
	 *               <code>https://gitea.domain.tld/api/v1</code>
	 * @param token  the API token used for API access
	 */
	public Gitea(URL server, String token) {
		this.connection = new GiteaConnection(server, token);
	}

	/**
	 * Opens an issue in gitea.
	 * 
	 * @param repoOwner owner of the gitea repository
	 * @param repoName  name of the gitea repository
	 * @param issue     {@link Issue} to be oppened
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public void openIssue(String repoOwner, String repoName, Issue issue) throws MalformedURLException, IOException {
		connection.post(GiteaC.PATH_ISSUES.replace(repoOwner, repoName), issue.toString());
	}

	/**
	 * Queries IDs of all labels with the passed name.
	 * 
	 * @param repoOwner owner of the gitea repository
	 * @param repoName  name of the gitea repository
	 * @param labelName label name to get IDs from
	 * @return {@link LinkedList} with IDs
	 * @throws JSONException
	 * @throws IOException
	 */
	public LinkedList<Integer> getLabelidsByName(String repoOwner, String repoName, String labelName)
			throws JSONException, IOException {
		LinkedList<Integer> ids = new LinkedList<Integer>();

		// Extract all ids of name matching labels
		JSONArray result = new JSONArray(connection.get(GiteaC.PATH_LABELS.replace(repoOwner, repoName)));
		result.forEach(o -> {
			JSONObject json = (JSONObject) o;
			if (json.getString(JsonC.NAME).equals(labelName))
				ids.add(json.getInt(JsonC.ID));
		});

		return ids;
	}
}

package app.pott.barista.aer;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.tomcat.jdbc.pool.DataSource;

import app.pott.barista.Constants.AerC;
import app.pott.barista.Constants.ConfigC;
import app.pott.barista.Constants.GiteaC;
import app.pott.barista.config.Config;
import app.pott.barista.database.DatabaseAdapter;
import app.pott.barista.gitea.Gitea;
import app.pott.barista.gitea.Issue;
import app.pott.barista.log.Log;
import app.pott.barista.servlet.issue.ReporterResult;

/**
 * Automatic Error Reporter. Global component for gitea error reporting.
 * 
 * @author epileptic
 */
public class Aer {
	private Config cfg;
	private Gitea gitea;
	private DataSource dataSource;

	public Aer(Config cfg, DataSource aerDb) throws MalformedURLException {
		this.cfg = cfg;
		this.dataSource = aerDb;

		// Instantiate Gitea
		gitea = new Gitea(new URL(cfg.get(ConfigC.GITEA_API_URL)), cfg.get(ConfigC.GITEA_API_TOKEN));
		Log.d("Aer ready!", this);
	}

	/**
	 * Opens an issue in Gitea if this error is unknown.
	 * 
	 * @param stacktrace the stacktrace that will be posted
	 * @param thread     the thread in that the error occurred
	 * @param apiLevel   the android API level in that the exception occurred
	 * @param appVersion the kaffeepott application version on which the exception
	 *                   occurred
	 * @return {@link ReporterResult}
	 */
	public ReporterResult reportIssue(String stacktrace, String thread, String apiLevel, String appVersion) {
		try (Connection c = dataSource.getConnection()) {
			DatabaseAdapter db = new DatabaseAdapter(c);

			// Build title message
			String title = GiteaC.ISSUE_TITLE.replace(thread, apiLevel, appVersion);
			String messageContent = "```\n" + stacktrace + "```";

			// Check if duplicate
			ResultSet reportsResult = db.cachedQuery(AerC.GET_REPORTS, stacktrace, thread, apiLevel, appVersion);
			boolean existent = reportsResult.next();
			Log.d("Report existent: " + existent, this);

			if (!existent) {
				// Add to db
				db.cachedQuery(AerC.INSERT_REPORT, stacktrace, thread, apiLevel, appVersion);
				Log.d("Added report to db", this);

				// Open issue in gitea
				gitea.openIssue(cfg.get(ConfigC.GITEA_REPO_OWNER), cfg.get(ConfigC.GITEA_REPO_NAME),
						new Issue(title, messageContent, gitea.getLabelidsByName(cfg.get(ConfigC.GITEA_REPO_OWNER),
								cfg.get(ConfigC.GITEA_REPO_NAME), GiteaC.LABEL_REPORTER).get(0)));
				Log.d("Opened gitea issue", this);

				return ReporterResult.OK;
			} else {
				return ReporterResult.SKIP;
			}
		} catch (Throwable e) {
			return ReporterResult.ERROR;
		}
	}
}

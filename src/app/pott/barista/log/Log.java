package app.pott.barista.log;

import java.io.PrintStream;
import java.sql.Timestamp;

import app.pott.barista.Constants.VersionC;

public class Log {

	private static boolean verbose;

	public static void setVerbose(boolean verbose) {
		Log.verbose = verbose;
	}

	/**
	 * Prints to the System.out stream if verbose is enabled.
	 * 
	 * @param message {@link String} message to be printed.
	 * @param source  the origin object that called this method. If {@link String}
	 *                the content will be printed. Otherwise it will get the class
	 *                name of the passed object
	 */
	public static void d(String message, Object source) {
		if (verbose) {
			String sourceString;
			if (source instanceof String)
				sourceString = (String) source;
			else
				sourceString = source.getClass().getName();

			sysprint(sourceString, message, System.out);
		}
	}

	/**
	 * Prints to the System.out stream.
	 * 
	 * @param message {@link String} message to be printed.
	 * @param source  the origin object that called this method. If {@link String}
	 *                the content will be printed. Otherwise it will get the class
	 *                name of the passed object
	 */
	public static void ok(String message, Object source) {
		String sourceString;
		if (source instanceof String)
			sourceString = (String) source;
		else
			sourceString = source.getClass().getName();

		sysprint(sourceString, message, System.out);
	}

	/**
	 * Prints to the System.err stream.
	 * 
	 * @param message {@link String} message to be printed
	 * @param source  the origin object that called this method. If {@link String}
	 *                the content will be printed. Otherwise it will get the class
	 *                name of the passed object
	 */
	public static void err(String message, Object source) {
		String sourceString;
		if (source instanceof String)
			sourceString = (String) source;
		else
			sourceString = source.getClass().getName();

		sysprint(sourceString, message, System.err);
	}

	/**
	 * Prints the exception to the System.err stream.
	 * 
	 * @param exception {@link Exception} that will be printed
	 * @param source    the origin object that called this method. If {@link String}
	 *                  the content will be printed. Otherwise it will get the class
	 *                  name of the passed object.
	 */
	public static void x(Exception exception, Object source) {
		String sourceString;
		if (source instanceof String)
			sourceString = (String) source;
		else
			sourceString = source.getClass().getName();

		sysprint(sourceString, "Exception occured", System.err);
		exception.printStackTrace();
	}

	private static void sysprint(String author, String message, PrintStream stream) {
		String stamp = new Timestamp(System.currentTimeMillis()).toString();
		if (stamp.length() < 23)
			stamp += "0";
		stream.println("[" + stamp + "]:[" + VersionC.VERSION + "] " + author + " > " + message);
	}
}

package app.pott.barista.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.http.RequestType;

public interface BaristaApi {
	public void call(HttpServletRequest request, HttpServletResponse response, RequestType type)
			throws BaristaException;
}

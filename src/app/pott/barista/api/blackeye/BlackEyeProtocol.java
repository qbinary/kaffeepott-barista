package app.pott.barista.api.blackeye;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.model.entities.Badge;
import app.pott.barista.database.model.entities.Cafe;
import app.pott.barista.database.model.entities.CafeProduct;
import app.pott.barista.database.model.entities.LocalizedProductType;
import app.pott.barista.database.model.entities.OpeningRange;
import app.pott.barista.database.model.entities.RatingMeta;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.util.json.XJsonArray;
import app.pott.barista.util.json.XJsonException;
import app.pott.barista.util.json.XJsonObject;

public class BlackEyeProtocol {

	public static final class BEPSerialize {
		public static XJsonObject buildCafe(Cafe cafe, List<CafeProduct> products, List<OpeningRange> ranges,
				List<Badge> badges, RatingMeta rating) {
			//// Root
			XJsonObject cafeJson = new XJsonObject();
			cafeJson.put(JsonC.CAFE_ID, cafe.getId());
			cafeJson.put(JsonC.CAFE_NAME, cafe.getName());
			cafeJson.put(JsonC.LONGITUDE, cafe.getLongitude());
			cafeJson.put(JsonC.LATITUDE, cafe.getLatitude());

			// Products
			XJsonArray productsJson = new XJsonArray();
			products.forEach(c -> productsJson.put(buildCafeProduct(c)));
			cafeJson.put(JsonC.PRODUCTS, productsJson);

			// Ranges
			XJsonArray rangesJson = new XJsonArray();
			ranges.forEach(c -> rangesJson.put(buildOpeningRange(c)));
			cafeJson.put(JsonC.OPENING_RANGES, rangesJson);

			// Badges
			XJsonArray badgesJson = new XJsonArray();
			badges.forEach(c -> badgesJson.put(c.getBadgeType()));
			cafeJson.put(JsonC.BADGES, badgesJson);

			// Rating
			cafeJson.put(JsonC.RATING_VALUE, rating.getAvg());
			cafeJson.put(JsonC.RATING_COUNT, rating.getRatingCount());

			return cafeJson;
		}

		public static XJsonObject buildLocalizedProductType(LocalizedProductType product) {
			XJsonObject productJson = new XJsonObject();
			productJson.put(JsonC.PRODUCT_TYPE_NAME, product.getTypeName());
			productJson.put(JsonC.PRODUCT_TYPE_NAME_LOCALIZED, product.getTypeNameLocalized());
			return productJson;
		}

		public static XJsonObject buildCafeProduct(CafeProduct product) {
			XJsonObject productJson = new XJsonObject();
			productJson.put(JsonC.PRODUCT_TYPE_NAME, product.getProductTypeName());
			productJson.put(JsonC.PRODUCT_SIZE, product.getSize());
			productJson.put(JsonC.PRODUCT_PRICE, product.getPrice());
			return productJson;
		}

		public static XJsonObject buildOpeningRange(OpeningRange range) {
			XJsonObject openingRangeJson = new XJsonObject();
			openingRangeJson.put(JsonC.DAY_OF_WEEK, range.getDay());

			Calendar openingCalendar = Calendar.getInstance();
			openingCalendar.setTime(range.getOpeningTime());
			openingRangeJson.put(JsonC.OPENING_HOUR, openingCalendar.get(Calendar.HOUR_OF_DAY));
			openingRangeJson.put(JsonC.OPENING_MINUTE, openingCalendar.get(Calendar.MINUTE));

			Calendar closingCalendar = Calendar.getInstance();
			closingCalendar.setTime(range.getClosingTime());
			openingRangeJson.put(JsonC.CLOSING_HOUR, closingCalendar.get(Calendar.HOUR_OF_DAY));
			openingRangeJson.put(JsonC.CLOSING_MINUTE, closingCalendar.get(Calendar.MINUTE));

			return openingRangeJson;
		}
	}

	public static final class BEPDeserialize {
		public static OpeningRange buildOpeningRange(XJsonObject range)
				throws BaristaException, ParseException, XJsonException {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			Timestamp openingTime = new Timestamp(
					format.parse(range.getString(JsonC.OPENING_HOUR + ":" + JsonC.OPENING_MINUTE)).getTime());
			Timestamp closingTime = new Timestamp(
					format.parse(range.getString(JsonC.CLOSING_HOUR + ":" + JsonC.CLOSING_MINUTE)).getTime());
			return new OpeningRange(range.getString(JsonC.DAY_OF_WEEK), openingTime, closingTime);
		}
	}
}

package app.pott.barista.api.blackeye;

public class BlackEyeConstants {
	public static final class JsonC {
		// General
		public static final String ERROR_MESSAGE = "error_message";
		public static final String CONTENT = "content";
		public static final String USER_ID = "user_id";
		public static final String LOCALE = "locale";

		// Cafe
		public static final String CAFE_ID = "cafe_id";
		public static final String CAFE_NAME = "cafe_name";

		// Location
		public static final String LONGITUDE = "longitude";
		public static final String LATITUDE = "latitude";
		public static final String DISTANCE = "distance";

		// Products
		public static final String PRODUCTS = "products";
		public static final String PRODUCT_TYPE_NAME = "product_type_name";
		public static final String PRODUCT_TYPE_NAME_LOCALIZED = "product_type_name_localized";
		public static final String PRODUCT_PRICE = "product_price";
		public static final String PRODUCT_SIZE = "product_size";
		public static final String LOCALIZED_PRODUCT_HASH = "localized_product_hash";

		// Ticket
		public static final String VIRTUAL_CAFE_ID = "virtual_cafe_id";
		public static final String TICKET_OPERATION = "ticket_operation";
		public static final String TICKET_ID = "ticket_id";
		public static final String TICKET_STATUS = "ticket_status";
		public static final String SELECT = "select";
		public static final String CHANGE = "change";

		// Badge
		public static final String BADGES = "badges";
		public static final String BADGE_TYPE_NAME = "badge_type_name";

		// Rating
		public static final String RATING_VALUE = "rating_value";
		public static final String RATING_COUNT = "rating_count";

		// Opening ranges
		public static final String OPENING_RANGES = "opening_ranges";
		public static final String OPENING_MINUTE = "opening_minute";
		public static final String OPENING_HOUR = "opening_hour";
		public static final String CLOSING_MINUTE = "closing_minute";
		public static final String CLOSING_HOUR = "closing_hour";
		public static final String DAY_OF_WEEK = "day_of_week";

		// Aer
		public static final String STACKTRACE = "stacktrace";
		public static final String THREAD = "thread";
		public static final String API_LEVEL = "api_level";
		public static final String APP_VERSION = "app_version";
		public static final String AER_RESULT = "aer_result";
	}
}

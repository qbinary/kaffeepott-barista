package app.pott.barista.api.blackeye.function.openticket;

import app.pott.barista.exceptions.BaristaBuilderIntegrityException;
import app.pott.barista.exceptions.Exf;

/**
 * Builder for {@link OpenTicketResult}
 * 
 * @author epileptic
 */
public class TicketResultBuilder {
	private Long ticketId;
	private Long virtualCafeId;

	public TicketResultBuilder() {
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public void setVirtualCafeId(Long virtualCafeId) {
		this.virtualCafeId = virtualCafeId;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public Long getVirtualCafeId() {
		return virtualCafeId;
	}

	public OpenTicketResult build() throws BaristaBuilderIntegrityException {
		if (ticketId == null)
			throw Exf.newBuilderIntegrityException(this.getClass());
		return new OpenTicketResult(ticketId, virtualCafeId);
	}
}

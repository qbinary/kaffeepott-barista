package app.pott.barista.api.blackeye.function;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.Constants;
import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.LocalizedProductType;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.KaffeePottLocale;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to check if product types of a and translation of a
 * specific language changed.
 * 
 * @author epileptic
 */
public class ProductHashFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.GET;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {
		XJsonObject contentResultJson = new XJsonObject();

		// Retrieve digest
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance(Constants.BLACK_EYE_GET_PROD_TYPE_HASH_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			throw new BaristaException(e, ErrorType.PANIC);
		}

		// Hash localized products
		try {
			List<LocalizedProductType> products = adm
					.getLocalizedProductTypes(new KaffeePottLocale(httpRequest.getParameter(JsonC.LOCALE)));
			products.forEach(p -> {
				digest.update(p.getTypeName().getBytes());
				digest.update(p.getTypeNameLocalized().getBytes());
			});
		} catch (SQLException e) {
			throw Exf.newWrappedInternal(e);
		}

		// Set result
		contentResultJson.put(JsonC.LOCALIZED_PRODUCT_HASH, Base64.getEncoder().encodeToString(digest.digest()));

		XJsonObject rootResultJson = new XJsonObject();
		rootResultJson.put(JsonC.CONTENT, contentResultJson);
		return rootResultJson;
	}

}

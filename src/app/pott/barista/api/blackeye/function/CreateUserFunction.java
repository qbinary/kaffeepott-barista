package app.pott.barista.api.blackeye.function;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.Constants.HttpC;
import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.User;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.Users;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to create and register a new user id.
 * 
 * @author epileptic
 */
public class CreateUserFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.GET;
	}

	@Override
	public XJson call(HttpServletRequest request, XJsonObject body, AbstractDataManager adm) throws BaristaException {
		XJsonObject contentJson = new XJsonObject();

		User user;
		try {
			user = new User(Users.generate(request.getHeader(HttpC.NGINX_REAL_IP_HEADER)), "::1");
			adm.addUser(user);
			contentJson.put(JsonC.USER_ID, user.getUserId());
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			throw new BaristaException(e, ErrorType.PANIC);
		} catch (SQLException e) {
			throw new BaristaException(e, ErrorType.INTERNAL);
		}

		XJsonObject resultJson = new XJsonObject();
		resultJson.put(JsonC.CONTENT, contentJson);
		return resultJson;
	}

}

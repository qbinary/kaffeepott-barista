package app.pott.barista.api.blackeye.function;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonObject;

/**
 * Interface for black eye API functions.
 * 
 * @author epileptic
 */
public interface BlackEyeApiFunction {
	/**
	 * @return which HTTP method is required for this function
	 */
	public HttpMethod getSupportedMethod();

	/**
	 * Method will be called if a function an API function gets used.
	 * 
	 * @param request Tomcat {@link HttpServletRequest}
	 * @param body    The HTTP body parsed as JSON. Null if body cannot be parsed
	 * @param adm     The {@link AbstractDataManager} only valid for this call.
	 * @return The HTTP answer body as {@link XJson}.
	 * @throws BaristaException can be thrown if an error occurs
	 */
	public XJson call(HttpServletRequest request, XJsonObject body, AbstractDataManager adm) throws BaristaException;
}

package app.pott.barista.api;

import app.pott.barista.api.asiatico.*;
import app.pott.barista.api.blackeye.*;

public enum ApiVersion {
	ASIATICO(new AsiaticoApi()), BLACK_EYE(new BlackEyeApi()), LATEST(ApiVersion.BLACK_EYE);

	private BaristaApi api;

	private ApiVersion(BaristaApi api) {
		this.api = api;
	}

	private ApiVersion(ApiVersion currentVersion) {
		this.api = currentVersion.getApi();
	}

	public BaristaApi getApi() {
		return api;
	}
}

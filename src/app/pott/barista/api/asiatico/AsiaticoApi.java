package app.pott.barista.api.asiatico;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import app.pott.barista.BaristaResources;
import app.pott.barista.Constants.AsiaticoProtocolC;
import app.pott.barista.Constants.ConfigC;
import app.pott.barista.Constants.HttpC;
import app.pott.barista.Constants.MimeC;
import app.pott.barista.api.BaristaApi;
import app.pott.barista.config.Config;
import app.pott.barista.database.DatabaseAdapter;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.http.RequestType;

public class AsiaticoApi implements BaristaApi {

	// Config
	private final Config config = BaristaResources.getInstance().getConfig();

	public AsiaticoApi() {
	}

	@Override
	public void call(HttpServletRequest request, HttpServletResponse response, RequestType type)
			throws BaristaException {
		try (Connection cnn = BaristaResources.getInstance().getKafffeepottDb().getConnection()) {
			// Initialize datamanager
			AbstractDataManager dm = new AbstractDataManager(new DatabaseAdapter(cnn));

			try {

				/*
				 * PLEASE DO NOT TALK ABOUT THE FOLLOWING. 1-month-ago-me thought that this
				 * protocol definition was good... For god sake! But I need to keep Barista
				 * compatible
				 */

				// Parameter to list
				LinkedList<String> paramList = new LinkedList<>();
				Enumeration<String> paramEnum = request.getParameterNames();
				while (paramEnum.hasMoreElements()) {
					paramList.add(paramEnum.nextElement());
				}

				// Dirt: check that parameter count is 'valid'
				if (paramList.size() != 2)
					throw Exf.newInvalidParameterCount(2, paramList.size());

				// Dirt²: Determine command parameter
				int commandIndex = 0;
				if (paramList.get(commandIndex).equals(AsiaticoProtocolC.VERSION_PARAMETER))
					commandIndex = 1;

				// Dirt: Determine command
				String command = paramList.get(commandIndex);
				String arguments = request.getParameter(paramList.get(commandIndex));
				JSONObject args = null;
				if (!command.equals(AsiaticoProtocolC.REGISTERUSER))
					args = new JSONObject(arguments);

				// Dirt³: Handle command
				RequestHandler handler = new RequestHandler(dm);
				switch (command) {
				case AsiaticoProtocolC.GETCAFES:
					sendJson(handler.getcafes(args).toString(), response);
					break;

				case AsiaticoProtocolC.GETPRODTYPEHASH:
					sendJson(handler.getProdTypeHash(args, config.get(ConfigC.DEFAULT_LOCALE)).toString(), response);
					break;

				case AsiaticoProtocolC.OPENTICKET:
					sendJson(handler.openTicket(args).toString(), response);
					break;

				case AsiaticoProtocolC.SETRATING:
					sendJson(handler.setRating(args).toString(), response);
					break;

				case AsiaticoProtocolC.GETPRODUCTS:
					sendJson(handler.getProducts(args, config.get(ConfigC.DEFAULT_LOCALE)).toString(), response);
					break;

				case AsiaticoProtocolC.REGISTERUSER:
					sendJson(handler.registerUser(request.getHeader(HttpC.NGINX_REAL_IP_HEADER)).toString(), response);
					break;

				default:
					throw Exf.newInvalidParameterCount(2, paramList.size());
				}

			} catch (SQLException e) {
				throw new BaristaException(e, ErrorType.INTERNAL);
			} catch (JSONException e) {
				throw Exf.newWrappedArgument(e);
			} catch (IOException e) {
				throw new BaristaException(e, ErrorType.PANIC);
			} catch (NoSuchAlgorithmException e) {
				throw new BaristaException(e, ErrorType.PANIC);
			}
		} catch (SQLException e) {
			throw new BaristaException(e, ErrorType.INTERNAL);
		}
	}

	private void sendJson(String message, HttpServletResponse response) throws IOException {

		// Set status stuff
		response.setHeader(HttpC.CONTENT_TYPE_HEADER, MimeC.APPLICATION_JSON);
		response.setStatus(HttpServletResponse.SC_OK);

		// Send message
		PrintWriter writer = response.getWriter();
		writer.append(message);
		writer.flush();

	}
}

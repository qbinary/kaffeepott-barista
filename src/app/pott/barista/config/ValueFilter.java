package app.pott.barista.config;

/**
 * Use in {@link Property} objects for value validation.
 * 
 * @author epileptic
 */
public interface ValueFilter {
	boolean isValueValid(String value);
}

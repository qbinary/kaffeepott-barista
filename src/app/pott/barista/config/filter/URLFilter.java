package app.pott.barista.config.filter;

import java.net.MalformedURLException;
import java.net.URL;

import app.pott.barista.config.ValueFilter;

public class URLFilter implements ValueFilter {

	@Override
	public boolean isValueValid(String value) {
		try {
			new URL(value);
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}

}

package app.pott.barista.config.filter;

import app.pott.barista.config.ValueFilter;

public class BooleanFilter implements ValueFilter {

	@Override
	public boolean isValueValid(String value) {
		try {
			Boolean.parseBoolean(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}

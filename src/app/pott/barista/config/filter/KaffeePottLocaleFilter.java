package app.pott.barista.config.filter;

import app.pott.barista.config.ValueFilter;
import app.pott.barista.util.KaffeePottLocale;

public class KaffeePottLocaleFilter implements ValueFilter {
	@Override
	public boolean isValueValid(String value) {
		try {
			new KaffeePottLocale(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}

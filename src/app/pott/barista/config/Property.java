package app.pott.barista.config;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import app.pott.barista.exceptions.Exf;
import app.pott.barista.exceptions.FilterRuleViolationException;
import app.pott.barista.log.Log;

/**
 * A class representing a configurable {@link Property}. In addition to that is
 * it possible to set a {@link ValueFilter} which will check the value every
 * time the <code>set()</code> method is called. Designed to be used with
 * {@link Config}.get(Property p);
 * 
 * @author epileptic
 */
public class Property {

	// Data pair
	private String key;
	private String value;

	// Filter
	private List<ValueFilter> filters;

	/**
	 * 
	 * @param key          key for this {@link Property}
	 * @param defaultValue initial value for this property
	 * @param filter       {@link ValueFilter}
	 * @throws FilterRuleViolationException if defaultValue does not match one of
	 *                                      the filters
	 */
	public Property(String key, String defaultValue, ValueFilter... filter) {
		this.filters = new LinkedList<ValueFilter>(Arrays.asList(filter));

		setKey(key);
		try {
			setValue(defaultValue);
		} catch (FilterRuleViolationException e) {
			Log.x(e, this);
		}
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Replaces the value if all {@link ValueFilter}s match.
	 * 
	 * @param value
	 * @throws FilterRuleViolationException if one of the {@link ValueFilter}s does
	 *                                      not match.
	 */
	public void setValue(String value) throws FilterRuleViolationException {

		// Check if value matches filters
		boolean valid = true;
		Class<? extends ValueFilter> lastFilter = null;
		for (ValueFilter f : filters) {

			// Check if valid
			valid &= f.isValueValid(value);

			// Set class of last filter (debugging)
			lastFilter = f.getClass();

			// Return if invalid
			if (!valid)
				return;

		}

		// Replace value if valid, otherwis throw exception
		if (valid)
			this.value = value;
		else
			throw Exf.newFilterRuleViolationException(lastFilter);
	}
}

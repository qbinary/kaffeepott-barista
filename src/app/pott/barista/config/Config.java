package app.pott.barista.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

import app.pott.barista.Constants.ConfigC;
import app.pott.barista.Constants.VersionC;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.FilterRuleViolationException;
import app.pott.barista.log.Log;

/**
 * Simple configuration system used all over Barista.
 * 
 * @author epileptic
 */
public class Config {

	// Config
	private final Properties configProperties;
	private final Path configFilePath;

	// Changes
	private boolean hasChanged = false;

	/**
	 * 
	 * @param configFilePath the config file path
	 * @throws FileNotFoundException if config file is inexistent
	 * @throws IOException           if <code>Properties.load(...)</code> throws an
	 *                               {@link IOException}
	 */
	public Config(Path configFilePath) throws FileNotFoundException, IOException {
		this.configFilePath = configFilePath;
		this.configProperties = new Properties();

		// Initial load
		load();
	}

	/**
	 * Tries to update the passed property corresponding to the underlying
	 * {@link Properties}. If config does not include key of property, it will put
	 * the current value of the {@link Property} into the config file. Otherwise the
	 * {@link Property} value will be replaced by the configurations content.
	 * 
	 * @param toUpdate {@link Property} to be updated
	 * @throws BaristaException
	 */
	public String get(Property toUpdate) {
		try {
			// Check if key already exists in config
			if (configProperties.containsKey(toUpdate.getKey())) {

				// If it does, try to update Property
				toUpdate.setValue(configProperties.getProperty(toUpdate.getKey()));

			} else {

				// If not, write default to config
				if (toUpdate.getValue() == null)
					configProperties.put(toUpdate.getKey(), ConfigC.CONFIG_DEFAULT_ERR);
				else
					configProperties.put(toUpdate.getKey(), toUpdate.getValue());
				hasChanged = true;

			}
		} catch (FilterRuleViolationException e) {
			Log.x(e, this);
		}
		update();
		return toUpdate.getValue();
	}

	/**
	 * Check for pending operations such as reloading the config file or saving
	 * changes to it. Called regularly by some {@link Config} methods.
	 * 
	 * @throws BaristaException
	 */
	public void update() {
		// Save config to disk if changed
		if (hasChanged) {
			store();
		} else {
			try {
				load();
			} catch (IOException e) {
				Log.x(e, this);
			}
		}
	}

	/**
	 * Stores the config file to disk
	 */
	private void store() {
		try {
			configProperties.store(new FileWriter(configFilePath.toFile()), VersionC.VERSION_STRING);
		} catch (IOException e) {
			Log.x(e, this);
		}
	}

	/**
	 * Load config file from disk
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void load() throws FileNotFoundException, IOException {
		configProperties.load(new FileInputStream(configFilePath.toFile()));
	}
}

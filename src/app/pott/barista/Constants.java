package app.pott.barista;

import app.pott.barista.config.Property;
import app.pott.barista.config.ValueFilter;
import app.pott.barista.config.filter.BooleanFilter;
import app.pott.barista.config.filter.KaffeePottLocaleFilter;
import app.pott.barista.config.filter.URLFilter;
import app.pott.barista.util.DynamicString;

/**
 * Constants for everything included in Barista
 * 
 * @author epileptic
 */
public class Constants {

	public static final String CONFIG_FILE_LOCATION = "/opt/barista/config.prp";
	public static final String CONFIG_FILE_HASH_ALGORITHM = "SHA-1";

	public static final int OPENING_RANGE_TICKET_LIMIT = 100;
	public static final String OPENING_RANGE_DATE_FORMAT = "HH:mm";

	public static final String ASIATICO_GET_PROD_TYPE_HASH_ALGORITHM = "SHA-224";
	public static final String ASIATICO_STRING_DEFAULT_ENCODING = "UTF-8";
	public static final String ASIATICO_REGISTER_USER_HASH_ALGORITHM = "SHA-512";

	public static final String BLACK_EYE_GET_PROD_TYPE_HASH_ALGORITHM = "SHA-224";

	public static final class VersionC {
		public static final String VERSION = "2.0.2";
		public static final String VERSION_CODENAME = "BLACK_EYE";
		public static final String VERSION_STRING = "v" + VERSION + "_" + VERSION_CODENAME;
		public static final boolean IS_BETA = VERSION.contains("beta");
	}

	public static final class DataSourceC {
		public static final String DB_URL = "jdbc:mariadb://" + PlaceholderC.IP + ":" + PlaceholderC.PORT + "/"
				+ PlaceholderC.DB;
	}

	public static final class AsiaticoProtocolC {
		public static final String VERSION_PARAMETER = "version";
		public static final String GETCAFES = "getcafes";
		public static final String GETPRODTYPEHASH = "getprodtypehash";
		public static final String OPENTICKET = "openticket";
		public static final String SETRATING = "setrating";
		public static final String GETPRODUCTS = "getproducts";
		public static final String REGISTERUSER = "registeruser";
	}

	public static final class BlackEyeProtocolC {
		public static final String FUNCTION_PARAMETER = "function";
	}

	public static final class HttpC {
		public static final String POST_METHOD = "POST";
		public static final String GET_METHOD = "GET";

		public static final String CONTENT_TYPE_HEADER = "Content-Type";
		public static final String NGINX_REAL_IP_HEADER = "X-Real-IP";
	}

	public static final class MimeC {
		public static final String PLAIN_TEXT = "text/plain";
		public static final String APPLICATION_JSON = "application/json";
	}

	public static final class AsiaticoTicketC {
		public static final String[] ADD_CAFE_SELECT = {};
		public static final String[] ADD_CAFE_CHANGE = { JsonC.NAME, JsonC.LATITUDE, JsonC.LONGITUDE };

		public static final String[] REMOVE_CAFE_SELECT = { JsonC.CAFE_ID };
		public static final String[] REMOVE_CAFE_CHANGE = {};

		public static final String[] CHANGE_CAFE_NAME_SELECT = { JsonC.CAFE_ID };
		public static final String[] CHANGE_CAFE_NAME_CHANGE = { JsonC.NAME };

		public static final String[] ADD_BADGE_SELECT = { JsonC.CAFE_ID };
		public static final String[] ADD_BADGE_CHANGE = { JsonC.BADGE };

		public static final String[] REMOVE_BADGE_SELECT = { JsonC.CAFE_ID };
		public static final String[] REMOVE_BADGE_CHANGE = { JsonC.BADGE };

		public static final String[] CHANGE_CAFE_OPENING_RANGES_SELECT = { JsonC.CAFE_ID };
		public static final String[] CHANGE_CAFE_OPENING_RANGES_CHANGE = { JsonC.OPENING_RANGES };

		public static final String[] CHANGE_PRODUCT_PRICE_SELECT = { JsonC.CAFE_ID, JsonC.NAME, JsonC.SIZE,
				JsonC.LOCALE };
		public static final String[] CHANGE_PRODUCT_PRICE_CHANGE = { JsonC.PRICE };

		public static final String[] DELETE_PRODUCT_SELECT = { JsonC.CAFE_ID, JsonC.NAME, JsonC.SIZE };
		public static final String[] DELETE_PRODUCT_CHANGE = {};

		public static final String[] ADD_PRODUCT_SELECT = { JsonC.CAFE_ID, JsonC.NAME, JsonC.SIZE };
		public static final String[] ADD_PRODUCT_CHANGE = { JsonC.PRICE };

		public static final String[] CHANGE_CAFE_LOCATION_SELECT = { JsonC.CAFE_ID };
		public static final String[] CHANGE_CAFE_LOCATION_CHANGE = { JsonC.LONGITUDE, JsonC.LATITUDE };
	}

	public static final class JsonC {

		// General
		public static final String NAME = "name";
		public static final String ID = "id";
		public static final String OK = "ok";
		public static final String DATA = "data";

		// Location
		public static final String LONGITUDE = "longitude";
		public static final String LATITUDE = "latitude";
		public static final String DISTANCE = "distance";

		// Opening ranges
		public static final String OPENING_RANGES = "opening_ranges";

		public static final String DAY_OF_WEEK = "day_of_week";
		public static final String OPENING_HOUR = "opening_hour";
		public static final String CLOSING_HOUR = "closing_hour";
		public static final String OPENING_MINUTE = "opening_minute";
		public static final String CLOSING_MINUTE = "closing_minute";

		// Rating
		public static final String RATING = "rating";
		public static final String RATING_COUNT = "ratingCount";

		// Products
		public static final String PRODUCTS = "products";

		public static final String TYPE_LOCALIZED = "type_localized";
		public static final String PRODUCT_TYPE_NAME = "type";
		public static final String SIZE = "size";
		public static final String PRICE = "price";

		// Badges
		public static final String BADGE = "badge";
		public static final String BADGES = "badges";

		// Cafe
		public static final String CAFE_ID = "cafe_id";
		public static final String USER_ID = "user_id";
		public static final String BADGE_TYPE_NAME = "badge_type_name";

		public static final String LOCALE = "locale";
		public static final String HASH = "hash";
		public static final String OPERATION = "operation";
		public static final String SELECT = "select";
		public static final String CHANGE = "change";

		// Ticket
		public static final String PSEUDO_ID = "pseudo_id";

		//// IssueBot
		public static final String STACKTRACE = "stacktrace";
		public static final String THREAD = "thread";
		public static final String API_LEVEL = "api_level";
		public static final String APP_VERSION = "app_version";
		public static final String ERR = "err";
	}

	public static final class ConfigC {
		public static final String CONFIG_DEFAULT_ERR = "ERR";

		//// Filter
		private static final ValueFilter BOOLEAN_VALUE_FILTER = new BooleanFilter();
		private static final ValueFilter KAFFEEPOTT_LOCALE_FILTER = new KaffeePottLocaleFilter();
		private static final ValueFilter URL_FILTER = new URLFilter();

		//// KEY
		private static final String ALLOW_GET_KEY = "ALLOW_GET";
		private static final String ALLOW_POST_KEY = "ALLOW_POST";

		private static final String DB_IP_KEY = "DB_IP";
		private static final String DB_PORT_KEY = "DB_PORT";
		private static final String DB_USER_KEY = "DB_USER";
		private static final String DB_PASSWORD_KEY = "DB_PASSWORD";

		private static final String GITEA_DB_NAME_KEY = "GITEA_DB_NAME";
		private static final String KAFFEEPOTT_DB_NAME_KEY = "KAFFEEPOTT_DB_NAME";

		private static final String DEFAULT_LOCALE_KEY = "DEFAULT_LOCALE";

		private static final String GITEA_API_URL_KEY = "GITEA_API_URL";
		private static final String GITEA_API_TOKEN_KEY = "GITEA_API_TOKEN";

		private static final String GITEA_REPO_OWNER_KEY = "GITEA_REPO_OWNER";
		private static final String GITEA_REPO_NAME_KEY = "GITEA_REPO_NAME";

		private static final String REPORTER_DB_NAME_KEY = "REPORTER_DB_NAME_KEY";

		private static final String VERBOSE_KEY = "VERBOSE";

		//// DEFAULTS
		private static final String ALLOW_GET_DEFAULT = "true";
		private static final String ALLOW_POST_DEFAULT = "false";

		private static final String DB_IP_DEFAULT = "pott.app";
		private static final String DB_PORT_DEFAULT = "3306";
		private static final String DB_USER_DEFAULT = "kaffeepott";
		private static final String DB_PASSWORD_DEFAULT = "";

		private static final String GITEA_DB_NAME_DEFAULT = "reports";
		private static final String KAFFEEPOTT_DB_NAME_DEFAULT = "kaffeepott";

		private static final String DEFAULT_LOCALE_DEFAULT = "en";

		private static final String GITEA_API_URL_DEFAULT = "https://git.pott.app/api/v1";
		private static final String GITEA_API_TOKEN_DEFAULT = "";

		private static final String GITEA_REPO_OWNER_DEFAULT = "kaffeepott";
		private static final String GITEA_REPO_NAME_DEFAULT = "Android-Client";

		private static final String REPORTER_DB_NAME_DEFAULT = "reporter";

		private static final String VERBOSE_DEFAULT = "false";

		//// PROPERTIES
		public static final Property ALLOW_GET = new Property(ALLOW_GET_KEY, ALLOW_GET_DEFAULT, BOOLEAN_VALUE_FILTER);

		public static final Property ALLOW_POST = new Property(ALLOW_POST_KEY, ALLOW_POST_DEFAULT,
				BOOLEAN_VALUE_FILTER);

		public static final Property DB_IP = new Property(DB_IP_KEY, DB_IP_DEFAULT);
		public static final Property DB_PORT = new Property(DB_PORT_KEY, DB_PORT_DEFAULT);
		public static final Property DB_USER = new Property(DB_USER_KEY, DB_USER_DEFAULT);
		public static final Property DB_PASSWORD = new Property(DB_PASSWORD_KEY, DB_PASSWORD_DEFAULT);

		public static final Property GITEA_DB_NAME = new Property(GITEA_DB_NAME_KEY, GITEA_DB_NAME_DEFAULT);
		public static final Property KAFFEEPOTT_DB_NAME = new Property(KAFFEEPOTT_DB_NAME_KEY,
				KAFFEEPOTT_DB_NAME_DEFAULT);

		public static final Property DEFAULT_LOCALE = new Property(DEFAULT_LOCALE_KEY, DEFAULT_LOCALE_DEFAULT,
				KAFFEEPOTT_LOCALE_FILTER);

		public static final Property GITEA_API_URL = new Property(GITEA_API_URL_KEY, GITEA_API_URL_DEFAULT, URL_FILTER);
		public static final Property GITEA_API_TOKEN = new Property(GITEA_API_TOKEN_KEY, GITEA_API_TOKEN_DEFAULT);

		public static final Property GITEA_REPO_OWNER = new Property(GITEA_REPO_OWNER_KEY, GITEA_REPO_OWNER_DEFAULT);
		public static final Property GITEA_REPO_NAME = new Property(GITEA_REPO_NAME_KEY, GITEA_REPO_NAME_DEFAULT);

		public static final Property REPORTS_DB_NAME = new Property(REPORTER_DB_NAME_KEY, REPORTER_DB_NAME_DEFAULT);

		public static final Property VERBOSE = new Property(VERBOSE_KEY, VERBOSE_DEFAULT, BOOLEAN_VALUE_FILTER);
	}

	public static final class PlaceholderC {
		public static final String NAME = "?NAME";
		public static final String FURTHER = "?FURTHER";
		public static final String CONTENT = "?CONTENT";

		public static final String EXPECTED = "?EXPECTED";
		public static final String ACTUAL = "?ACTUAL";

		public static final String SELECT = "?SELECT";
		public static final String CHANGE = "?CHANGE";

		public static final String IP = "?IP";
		public static final String PORT = "?PORT";
		public static final String DB = "?DB";
		public static final String METHOD = "?METHOD";
		public static final String FUNCTION = "?FUNCTION";
	}

	public static final class ExceptionC {
		public static final String FILTER_RULE_VIOLATION = "Filter of class '" + PlaceholderC.NAME + "' did not match";

		public static final String SECURITY_VIOLATION = "Security component '" + PlaceholderC.NAME
				+ "' encountered a problem! (" + PlaceholderC.FURTHER + ")";

		public static final String HTTP_MISSING_PARAMETER = "HTTP request parameter '" + PlaceholderC.NAME
				+ "' is missing";

		public static final String HTTP_INVALID_PARAMETER_VALUE = "HTTP request parameter '" + PlaceholderC.NAME
				+ "' has an invalid value '" + PlaceholderC.CONTENT + "'";

		public static final String HTTP_INVALID_PARAMETER_COUNT = "HTTP request parameter count invalid! Expected "
				+ PlaceholderC.EXPECTED;

		public static final String LOCALE_FORMAT_VIOLATION = "Format does not match our format: "
				+ PlaceholderC.CONTENT;

		public static final String OPENTICKET_INVALID_PARAMETER = "openticket received invalid parameter. Select needs ("
				+ PlaceholderC.SELECT + "); Change needs (" + PlaceholderC.CHANGE + ")";

		public static final String INVALID_METHOD_FUNCTION_COMBINATION = "Invalid http method (" + PlaceholderC.METHOD
				+ ") for used function";
	}

	public static final class TextC {
		public static final String GET_REQUEST_DEBUG = "GET received.";
		public static final String POST_REQUEST_DEBUG = "POST received.";

		public static final String BARISTA_RESOURCES_INIT_ERROR = "CRITICAL ERROR OCCURED! CANNOT PROCEED EXECUTION!";
		public static final String INVALID_TICKET_OPERATION_ERR = "Invalid ticket operation";
	}

	public static final class DbKeyC {

		// General
		public static final String NAME = "Name";
		public static final String ID = "ID";

		// Location
		public static final String LOCATION_LON = "LocationLon";
		public static final String LOCATION_LAT = "LocationLat";

		// Rating stuff
		public static final String RATING = "Rating";

		// Time stuff
		public static final String DAY_OF_WEEK = "DayOfWeek";
		public static final String OPENING_TIME = "OpeningTime";
		public static final String CLOSING_TIME = "ClosingTime";

		// User stuff
		public static final String USER_ID = "UserID";

		// Product stuff
		public static final String PRODUCT_TYPE_NAME = "ProductTypeName";
		public static final String SIZE = "Size";
		public static final String PRICE = "Price";

		// Badge Stuff
		public static final String BADGE_TYPE = "Type";

		// Ticket stuff
		public static final String AFFECTED_CAFE_ID = "AffectedCafeId";
		public static final String AFFECTED_PRODUCT_NAME = "AffectedProductName";
		public static final String AFFECTED_PRODUCT_SIZE = "AffectedProductSize";
		public static final String NEW_BADGE_TYPE = "NewBadgeType";
		public static final String NEW_LOCATION_LON = "NewLocationLon";
		public static final String NEW_LOCATION_LAT = "NewLocationLat";
		public static final String NEW_CAFE_NAME = "NewCafeName";
		public static final String NEW_PRODUCT_PRICE = "NewProductPrice";
		public static final String HAS_OPENING_CHANGE = "HasOpeningChange";
		public static final String MODE = "Mode";
		public static final String COMPANION_TYPE = "CompanionType";
		public static final String ACCEPTED = "Accepted";
	}

	public static final class TicketC {
		public static final String MODE_ADD = "ADD";
		public static final String MODE_REMOVE = "REMOVE";
		public static final String MODE_MODIFY = "MODIFY";
	}

	public static final class QueriesC {
		public static final class AsiaticoQ {
			// Load
			public static final String LOAD_CAFES_LOCATION_RANGE = "SELECT * FROM Cafe WHERE locationLon < ? AND locationLon > ? AND locationLat < ? AND locationLat > ?";
			public static final String LOAD_OPENING_RANGES_FOR_CAFE_ID = "SELECT * FROM OpeningRange WHERE CafeID = ?";
			public static final String LOAD_RATINGS_FOR_CAFE_ID = "SELECT * FROM CafeRating WHERE CafeID = ?";
			public static final String LOAD_PRODUCTS_FOR_CAFE_ID = "SELECT * FROM CafeProduct WHERE CafeID = ?";
			public static final String LOAD_BADGES_FOR_CAFE_ID = "SELECT * FROM Badge WHERE CafeID = ?";
			public static final String LOAD_LOCALIZED_PRODUCT_TYPES = "SELECT Name, Name_" + PlaceholderC.CONTENT
					+ " FROM ProductType";

			// Ticket
			public static final String TICKET_ADD_CAFE = "INSERT INTO Ticket (UserID, AffectedCafeId, NewCafeName, NewLocationLon, NewLocationLat, Mode, CompanionType) VALUES (?, ?, ?, ?, ?, 'add', ?)";
			public static final String TICKET_REMOVE_CAFE = "INSERT INTO Ticket (UserID, AffectedCafeID, Mode, CompanionType) VALUES (?, ?, 'delete', ?)";
			public static final String TICKET_CHANGE_CAFE_NAME = "INSERT INTO Ticket (UserID, AffectedCafeID, NewCafeName, Mode, CompanionType) VALUES (?, ?, ?, 'modify', ?)";
			public static final String TICKET_CHANGE_CAFE_LOCATION = "INSERT INTO Ticket (UserID, AffectedCafeID, NewLocationLat, NewLocationLon, Mode, CompanionType) VALUES (?, ?, ?, ?, 'modify', ?)";
			public static final String TICKET_CHANGE_PRODUCT_PRICE = "INSERT INTO Ticket (UserID, AffectedCafeID, AffectedProductName, AffectedProductSize, NewProductPrice, Mode, CompanionType) VALUES (?, ?, ?, ?, ?, 'modify', ?)";
			public static final String TICKET_DELETE_PRODUCT = "INSERT INTO Ticket (UserID, AffectedCafeID, AffectedProductName, AffectedProductSize, Mode, CompanionType) VALUES (?, ?, ?, ?, 'delete', ?)";
			public static final String TICKET_ADD_PRODUCT = "INSERT INTO Ticket (UserID, AffectedCafeID, AffectedProductName, AffectedProductSize, NewProductPrice, Mode, CompanionType) VALUES (?, ?, ?, ?, ?, 'add', ?)";

			public static final String TICKET_ADD_BADGE = "INSERT INTO Ticket (UserID, AffectedCafeID, NewBadgeType, Mode, CompanionType) VALUES (?, ?, ?, 'add', ?)";
			public static final String TICKET_REMOVE_BADGE = "INSERT INTO Ticket (UserID, AffectedCafeID, NewBadgeType, Mode, CompanionType) VALUES (?, ?, ?, 'delete', ?)";

			public static final String TICKET_CHANGE_CAFE_OPENING_RANGES = "INSERT INTO Ticket (UserID, AffectedCafeID, HasOpeningChange, Mode, CompanionType) VALUES (?, ?, true, 'modify', ?)";
			public static final String RANGE_CHANGE_CAFE_OPENING_RANGES = "INSERT INTO NewOpeningRange (TicketID, DayOfWeek, OpeningTime, ClosingTime) VALUES (?, ?, ?, ?)";

			// Util
			public static final String UTIL_COUNT_CAFE_BY_ID = "SELECT COUNT(*) FROM Cafe WHERE ID = ?";
			public static final String UTIL_COUNT_TICKET_BY_CAFE_ID = "SELECT COUNT(*) FROM Ticket WHERE AffectedCafeID = ?";

			public static final String RATING_SET = "REPLACE INTO CafeRating (CafeID, UserID, Rating) VALUES (?, ?, ?)";
			public static final String USER_REGISTER = "INSERT INTO User (ID, IP, CreationDate) VALUES (?, ?, NOW())";
			public static final String LOAD_TICKET_BY_ID = "SELECT * FROM Ticket WHERE ID = ? LIMIT 1";
			public static final String LOAD_NEW_OPENING_RANGES = "SELECT * FROM NewOpeningRange WHERE TicketID = ?";
		}
	}

	public static final class GiteaC {
		public static final DynamicString PATH_ISSUES = new DynamicString("/repos/{}/{}/issues");
		public static final DynamicString PATH_LABELS = new DynamicString("/repos/{}/{}/labels");

		public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
		public static final DynamicString AUTHORIZATION_HEADER_CONTENT = new DynamicString("token {}");

		public static final String ACCEPT_HEADER_NAME = "accept";
		public static final String ACCEPT_HEADER_CONTENT = MimeC.APPLICATION_JSON;

		public static final String CONTENT_TYPE_HEADER_NAME = HttpC.CONTENT_TYPE_HEADER;
		public static final String CONTENT_TYPE_HEADER_CONTENT = MimeC.APPLICATION_JSON;

		public static final DynamicString ISSUE_TITLE = new DynamicString(
				"App crash reported in thread '{}' on api level '{}'. App version code '{}' ");

		public static final String JSON_NAME = "name";
		public static final String JSON_ID = "id";
		public static final String JSON_OWNER = "owner";
		public static final String JSON_REPO = "repo";
		public static final String JSON_TITLE = "title";
		public static final String JSON_BODY = "body";
		public static final String JSON_LABELS = "labels";

		public static final String LABEL_REPORTER = "Reporter";
	}

	public static final class AerC {
		public static final String REPORT_PARAMETER = "report";

		public static final String GET_REPORTS = "SELECT * FROM Report WHERE Stacktrace = ? AND Thread = ? AND ApiLevel = ? AND AppVersion = ? ";
		public static final String INSERT_REPORT = "INSERT INTO Report (Stacktrace, Thread, ApiLevel, AppVersion) VALUES (?, ?, ?, ?)";

		public static final String STACKTRACE = "stacktrace";
		public static final String THREAD = "thread";
		public static final String API_LEVEL = "api_level";
		public static final String APP_VERSION = "app_version";
	}
}

package app.pott.barista;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.tomcat.jdbc.pool.DataSource;

import app.pott.barista.Constants.ConfigC;
import app.pott.barista.Constants.TextC;
import app.pott.barista.aer.Aer;
import app.pott.barista.config.Config;
import app.pott.barista.database.DataSourceFactory;
import app.pott.barista.log.Log;
import app.pott.barista.security.MultiComponent;
import app.pott.barista.security.SecurityComponent;
import app.pott.barista.security.components.GenuitSecurity;
import app.pott.barista.security.components.LogSecurity;

/**
 * Manager for global resources like database connections or configurations.
 * 
 * @author epileptic
 */
public class BaristaResources {

	// Singleton instance
	private static BaristaResources instance;

	// Resources
	private Config config;
	private SecurityComponent security;

	private DataSource kafffeepottDb;

	private Aer aer;

	private BaristaResources() {

		// Setup config
		try {
			// Make sure file exists
			Path cfgPath = Paths.get(Constants.CONFIG_FILE_LOCATION);
			if (!Files.isDirectory(cfgPath.getParent()))
				Files.createDirectories(cfgPath.getParent());
			if (!Files.exists(cfgPath))
				Files.createFile(cfgPath);

			this.config = new Config(cfgPath);
		} catch (IOException e) {
			Log.x(e, this);
			Log.err(TextC.BARISTA_RESOURCES_INIT_ERROR, this);
		}

		// Check for debug log mode
		Log.setVerbose(Boolean.parseBoolean(config.get(ConfigC.VERBOSE)));

		// Setup Genuit
		this.security = new MultiComponent(new GenuitSecurity(), new LogSecurity());

		// Setup db
		String ip = config.get(ConfigC.DB_IP);
		String port = config.get(ConfigC.DB_PORT);
		String dbUser = config.get(ConfigC.DB_USER);
		String dbPassword = config.get(ConfigC.DB_PASSWORD);

		kafffeepottDb = DataSourceFactory.newMariaDataSource(ip, port, config.get(ConfigC.KAFFEEPOTT_DB_NAME), dbUser,
				dbPassword);

		try {
			aer = new Aer(config, DataSourceFactory.newMariaDataSource(ip, port, config.get(ConfigC.REPORTS_DB_NAME),
					dbUser, dbPassword));
		} catch (MalformedURLException e) {
			Log.x(e, this);
			Log.err(TextC.BARISTA_RESOURCES_INIT_ERROR, this);
		}
	}

	public static synchronized BaristaResources getInstance() {
		if (instance == null)
			instance = new BaristaResources();
		return instance;
	}

	public Config getConfig() {
		return config;
	}

	public SecurityComponent getSecurity() {
		return security;
	}

	public DataSource getKafffeepottDb() {
		return kafffeepottDb;
	}

	public Aer getAer() {
		return aer;
	}
}

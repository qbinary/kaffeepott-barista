package app.pott.barista.util;

public enum HttpMethod {
	GET, POST, PUT
}

package app.pott.barista.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import app.pott.barista.Constants.QueriesC.AsiaticoQ;
import app.pott.barista.database.DatabaseAdapter;

/**
 * Toolkit for generating unique IDs for barista tickets.
 * 
 * @author epileptic
 */
public class IDs {
	private IDs() {
	}

	public static long getRandomValidLong(DatabaseAdapter db) throws SQLException {
		long random;
		do {
			Random rand = new Random(System.nanoTime());
			random = rand.nextLong();
		} while (!isValid(db, random));
		return random;
	}

	private static boolean isValid(DatabaseAdapter db, long l) throws SQLException {

		// Count cafes with id
		ResultSet resultCafe = db.cachedQuery(AsiaticoQ.UTIL_COUNT_CAFE_BY_ID, l);
		resultCafe.next();

		// Count ticket pointing to id
		ResultSet resultTicket = db.cachedQuery(AsiaticoQ.UTIL_COUNT_TICKET_BY_CAFE_ID, l);
		resultTicket.next();

		// Check that both counts are 0
		return resultCafe.getInt(1) == 0 && resultTicket.getInt(1) == 0;

	}
}

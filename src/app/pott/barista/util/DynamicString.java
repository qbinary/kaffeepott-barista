package app.pott.barista.util;

public class DynamicString {

	private String content;

	public DynamicString(String content) {
		this.content = content;
	}

	public String replace(String... strings) {
		for (final String s : strings)
			content = content.replaceFirst("\\{\\}", s);
		return content;
	}
}

package app.pott.barista.util;

import app.pott.barista.BaristaResources;
import app.pott.barista.Constants.ConfigC;
import app.pott.barista.exceptions.BaristaException;

public class KaffeePottLocale {

	private String localeString;

	public KaffeePottLocale(String localeString) throws BaristaException {
		if (!localeString.matches("[a-z]+") || localeString.length() != 2)
			localeString = BaristaResources.getInstance().getConfig().get(ConfigC.DEFAULT_LOCALE);
		else
			this.localeString = localeString;
	}

	@Override
	public String toString() {
		return localeString;
	}
}

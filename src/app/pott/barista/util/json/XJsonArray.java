package app.pott.barista.util.json;

import org.json.JSONArray;
import org.json.JSONObject;

public class XJsonArray implements XJson {

	JSONArray content;

	public XJsonArray() {
		content = new JSONArray();
	}

	public XJsonArray(String parse) {
		content = new JSONArray(parse);
	}

	public void put(Object o) {
		if (o instanceof XJsonObject)
			o = new JSONObject(((XJsonObject) o).toJsonString());
		else if (o instanceof XJsonArray)
			o = new JSONArray(((XJsonArray) o).toJsonString());
		content.put(o);
	}

	public String getString(int index) {
		return content.getString(index);
	}

	public int size() {
		return content.length();
	}

	@Override
	public String toJsonString() {
		return content.toString();
	}

	public XJsonObject getJsonObject(int index) {
		return new XJsonObject(content.getJSONObject(index).toString());
	}

}

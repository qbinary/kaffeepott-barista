package app.pott.barista.util.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class XJsonObject implements XJson {

	private JSONObject content;

	public XJsonObject(String jsonString) {
		content = new JSONObject(jsonString);
	}

	public XJsonObject() {
		content = new JSONObject();
	}

	public String getString(String key) throws XJsonException {
		try {
			return content.getString(key);
		} catch (JSONException e) {
			throw new XJsonException(e);
		}
	}

	public int getInt(String key) throws XJsonException {
		try {
			return content.getInt(key);
		} catch (JSONException e) {
			throw new XJsonException(e);
		}
	}

	public double getDouble(String key) throws XJsonException {
		try {
			return content.getDouble(key);
		} catch (JSONException e) {
			throw new XJsonException(e);
		}
	}

	public boolean has(String key) throws XJsonException {
		try {
			return content.has(key);
		} catch (JSONException e) {
			throw new XJsonException(e);
		}
	}

	public void put(String key, Object value) {
		if (value instanceof XJsonObject)
			value = new JSONObject(((XJsonObject) value).toJsonString());
		else if (value instanceof XJsonArray)
			value = new JSONArray(((XJsonArray) value).toJsonString());
		content.put(key, value);
	}

	@Override
	public String toJsonString() {
		return content.toString();
	}

	public long getLong(String key) {
		return content.getLong(key);
	}

	public XJsonObject getJsonObject(String key) {
		return new XJsonObject(content.getJSONObject(key).toString());
	}

	public XJsonArray getArray(String key) {
		return new XJsonArray(content.getJSONArray(key).toString());
	}
}

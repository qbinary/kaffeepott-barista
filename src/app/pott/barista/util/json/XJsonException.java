package app.pott.barista.util.json;

public class XJsonException extends Exception {

	private static final long serialVersionUID = 7633790531291434932L;

	public XJsonException(String message) {
		super(message);
	}

	public XJsonException(Throwable t) {
		super(t);
	}
}

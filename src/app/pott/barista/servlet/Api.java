package app.pott.barista.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.BaristaResources;
import app.pott.barista.Constants.AsiaticoProtocolC;
import app.pott.barista.Constants.HttpC;
import app.pott.barista.Constants.MimeC;
import app.pott.barista.api.ApiVersion;
import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.http.RequestType;
import app.pott.barista.log.Log;
import app.pott.barista.security.SecurityComponent;
import app.pott.barista.util.json.XJsonObject;

/**
 * API component of Barista
 * 
 * @author epileptic
 */
@WebServlet("/api")
public class Api extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// Resource manager
	private BaristaResources resources = BaristaResources.getInstance();

	// Resources
	private SecurityComponent security = resources.getSecurity();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		doAll(request, response, RequestType.GET);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		doAll(request, response, RequestType.POST);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) {
		doAll(request, response, RequestType.PUT);
	}

	private void doAll(HttpServletRequest request, HttpServletResponse response, RequestType type) {
		try {
			security.noteApiCall(request, response);

			// Parameter to list
			final List<String> parameterList = new LinkedList<>();
			Enumeration<String> reqParam = request.getParameterNames();
			while (reqParam.hasMoreElements())
				parameterList.add(reqParam.nextElement());

			// Debug out parameter
			for (final String param : parameterList)
				Log.d(param + "=" + request.getParameter(param), this);

			// Determine requested version
			String versionCode = request.getParameter(AsiaticoProtocolC.VERSION_PARAMETER);
			if (versionCode == null)
				throw Exf.newMissingUrlParameter(AsiaticoProtocolC.VERSION_PARAMETER);

			// Try to parse versionCode
			ApiVersion apiVersion = null;
			try {
				apiVersion = ApiVersion.valueOf(versionCode);
			} catch (IllegalArgumentException e) {
				throw Exf.newInvalidUrlParameterValue(AsiaticoProtocolC.VERSION_PARAMETER, versionCode);
			}

			// API call
			try {
				apiVersion.getApi().call(request, response, type);
			} catch (Throwable e) {
				throw new BaristaException(e, ErrorType.PANIC);
			}

			// Clean up
			System.gc();

		} catch (BaristaException e) {
			respondException(e, response);
			Log.x(e, this);
		}
	}

	private void respondException(BaristaException exception, HttpServletResponse response) {

		// Set response code 500
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

		// Send plain text type
		response.setHeader(HttpC.CONTENT_TYPE_HEADER, MimeC.PLAIN_TEXT);

		// Create result json
		XJsonObject result = new XJsonObject();
		result.put(JsonC.ERROR_MESSAGE, exception.toString());

		try {
			// Send response
			PrintWriter writer = response.getWriter();
			writer.append(result.toJsonString());
		} catch (IOException e) {
			Log.x(e, this);
		}
	}
}

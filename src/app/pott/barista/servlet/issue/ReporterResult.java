package app.pott.barista.servlet.issue;

public enum ReporterResult {
	OK, SKIP, ERROR
}

package app.pott.barista.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import app.pott.barista.BaristaResources;
import app.pott.barista.Constants.AerC;
import app.pott.barista.Constants.JsonC;
import app.pott.barista.Constants.MimeC;
import app.pott.barista.log.Log;
import app.pott.barista.servlet.issue.ReporterResult;

/**
 * Automatic error reporting server component
 * 
 * @author epileptic
 */
@WebServlet("/reporter")
public class IssueBot extends HttpServlet {
	private static final long serialVersionUID = -5286349806217559458L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doAll(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doAll(request, response);
	}

	private void doAll(HttpServletRequest request, HttpServletResponse response) {
		// Parse report data
		String report = request.getParameter(AerC.REPORT_PARAMETER);
		JSONObject args = new JSONObject(report);
		Log.d("Got report: " + report, this);

		// Get report information
		String stacktrace = args.getString(JsonC.STACKTRACE);
		String thread = args.getString(JsonC.THREAD);
		String apiLevel = String.valueOf(args.getLong(JsonC.API_LEVEL));
		String appVersion = String.valueOf(args.getLong(JsonC.APP_VERSION));

		ReporterResult result = BaristaResources.getInstance().getAer().reportIssue(stacktrace, thread, apiLevel,
				appVersion);

		// Build result json
		JSONObject resultJson = new JSONObject();
		resultJson.put(JsonC.OK, result.toString());

		// Send result
		response.setContentType(MimeC.APPLICATION_JSON);
		try {
			response.getWriter().append(resultJson.toString());
		} catch (IOException e) {
			Log.x(e, this);
		}
	}
}

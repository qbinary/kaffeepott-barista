package app.pott.barista.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.Constants.VersionC;

@WebServlet("/info")
public class Info extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String BETA = "beta";
	private static final String NOT_BETA = "muted";

	private static final String SPLASH_START = "<!doctype html><meta content='text/html; charset=UTF-8' http-equiv=content-type><meta content='width=device-width,initial-scale=0.8,maximum-scale=0.8,user-scalable=0' name=viewport><title>KaffeePott Barista</title><link href=https://pott.app/main.css rel=stylesheet><link href=https://pott.app/kp-t.svg rel=icon type=image/svg><div class='";
	private static final String SPLASH_END = " one-pane section'><div class='colour foreground has-content pane'><div class='central title'><span class=barista-logo><img src=https://pott.app/kp-s.svg>Barista<hr></span></div><div class='central actions' style=font-size:150%>Version: <code>"
			+ VersionC.VERSION_STRING
			+ "</code></div><span class='content two-rows'><span style=max-width:30rem;margin-right:2rem><span><h3>What is KaffeePott?</h3><span style=margin-left:1.5em;margin-right:1.5em lang=en>KaffeePott is an app that allows you to quickly find cafés.<br><a class=plain href=https://pott.app/ >&#xf35d;KaffeePott Homepage</a> </span></span></span><span style=margin-left:2rem;max-width:30rem><span style=padding:0><h3>What is Barista?</h3><span style=margin-left:1.5em;margin-right:1.5em lang=en>Barista is KaffeePott's application server.<br><a class=plain href=https://git.pott.app/kaffeepott/barista-server >&#xf35d;View Barista's Source Code</a></span></span></span></span></div></div><script src=https://pott.app/iconify.js></script><script src=https://pott.app/hesitant.js></script>";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		StringBuilder page = new StringBuilder(SPLASH_START);
		if (VersionC.IS_BETA)
			page.append(BETA);
		else
			page.append(NOT_BETA);
		page.append(SPLASH_END);

		response.setContentType("text/html");
		response.getWriter().append(page.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

}

package app.pott.barista.exceptions;

import java.io.IOException;

import app.pott.barista.api.blackeye.function.openticket.TicketResultBuilder;
import app.pott.barista.config.ValueFilter;

public class Exf {
	public static BaristaHttpParameterException newMissingUrlParameter(String parameter) {
		return new BaristaHttpParameterException("URL parameter '" + parameter + "' is missing");
	}

	public static BaristaHttpParameterException newInvalidUrlParameterValue(String parameterKey,
			String parameterValue) {
		return new BaristaHttpParameterException(
				"Parameter '" + parameterKey + "' has invalid value '" + parameterValue + "'");
	}

	public static BaristaException newWrappedInternal(Throwable t) {
		return new BaristaException(t, ErrorType.INTERNAL);
	}

	public static BaristaException newWrappedArgument(Throwable t) {
		return new BaristaException(t, ErrorType.ARGUMENT);
	}

	public static BaristaHttpParameterException newInvalidParameterCount(int should, int actual) {
		return new BaristaHttpParameterException(
				"The parameter count should be '" + should + "' but it is '" + actual + "'");
	}

	public static FilterRuleViolationException newFilterRuleViolationException(
			Class<? extends ValueFilter> lastFilter) {
		return new FilterRuleViolationException("Filter rule violation: " + lastFilter.getSimpleName());
	}

	public static BaristaClientConnectionException newClientConnectionException(IOException e) {
		return new BaristaClientConnectionException(e);
	}

	public static BaristaBuilderIntegrityException newBuilderIntegrityException(
			Class<? extends TicketResultBuilder> class1) {
		return new BaristaBuilderIntegrityException(class1);
	}
}

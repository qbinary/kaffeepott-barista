package app.pott.barista.exceptions;

public class BaristaHttpParameterException extends BaristaException {

	private static final long serialVersionUID = -3307377741557906903L;

	public BaristaHttpParameterException(Throwable t) {
		super(t, ErrorType.ARGUMENT);
	}

	public BaristaHttpParameterException(String message) {
		super(message, ErrorType.ARGUMENT);
	}
}

package app.pott.barista.exceptions;

public class BaristaClientConnectionException extends BaristaException {

	private static final long serialVersionUID = 134030611074362786L;

	public BaristaClientConnectionException(Throwable t) {
		super(t, ErrorType.INTERNAL);
	}
}

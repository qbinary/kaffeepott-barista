package app.pott.barista.exceptions;

public enum ErrorType {
	ARGUMENT, INTERNAL, PANIC, SECURITY;
}

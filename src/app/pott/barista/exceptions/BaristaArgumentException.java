package app.pott.barista.exceptions;

public class BaristaArgumentException extends BaristaException {

	private static final long serialVersionUID = 723207297258891580L;

	public BaristaArgumentException(String msg) {
		super(msg, ErrorType.ARGUMENT);
	}

	public BaristaArgumentException(Throwable t) {
		super(t, ErrorType.ARGUMENT);
	}
}

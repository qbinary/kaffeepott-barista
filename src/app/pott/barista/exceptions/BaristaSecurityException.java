package app.pott.barista.exceptions;

public class BaristaSecurityException extends BaristaException {

	private static final long serialVersionUID = -6771583627538323557L;

	public BaristaSecurityException(String message) {
		super(message, ErrorType.SECURITY);
	}
}

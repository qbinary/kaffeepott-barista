package app.pott.barista.security;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.exceptions.BaristaSecurityException;

public class MultiComponent extends SecurityComponent {

	private List<SecurityComponent> securityComponents;

	public MultiComponent(SecurityComponent... components) {
		this.securityComponents = new LinkedList<SecurityComponent>(Arrays.asList(components));
	}

	@Override
	public void noteApiCall(HttpServletRequest request, HttpServletResponse response) throws BaristaSecurityException {
		for (final SecurityComponent i : securityComponents)
			i.noteApiCall(request, response);
	}

	@Override
	public void noteGetRequest(HttpServletRequest request, HttpServletResponse response)
			throws BaristaSecurityException {
		for (final SecurityComponent i : securityComponents)
			i.noteGetRequest(request, response);
	}

	@Override
	public void notePostRequest(HttpServletRequest request, HttpServletResponse response)
			throws BaristaSecurityException {
		for (final SecurityComponent i : securityComponents)
			i.notePostRequest(request, response);
	}
}

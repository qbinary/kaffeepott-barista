package app.pott.barista.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.exceptions.BaristaSecurityException;

/**
 * Handler for general barista events.
 * 
 * @author epileptic
 */
public abstract class SecurityComponent {
	public void noteGetRequest(HttpServletRequest request, HttpServletResponse response)
			throws BaristaSecurityException {
	}

	public void notePostRequest(HttpServletRequest request, HttpServletResponse response)
			throws BaristaSecurityException {
	}

	public void noteApiCall(HttpServletRequest request, HttpServletResponse response) throws BaristaSecurityException {
	}
}

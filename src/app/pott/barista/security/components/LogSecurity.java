package app.pott.barista.security.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.log.Log;
import app.pott.barista.security.SecurityComponent;

public class LogSecurity extends SecurityComponent {
	@Override
	public void noteApiCall(HttpServletRequest request, HttpServletResponse response) {
		Log.d("api call", this);
	}

	@Override
	public void noteGetRequest(HttpServletRequest request, HttpServletResponse response) {
		Log.d("get request", this);
	}

	@Override
	public void notePostRequest(HttpServletRequest request, HttpServletResponse response) {
		Log.d("post request", this);
	}
}

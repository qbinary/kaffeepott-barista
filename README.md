# Kaffeepott's Barista
Barista is the application server of the Kaffeepott android app project.  
> Kaffeepott is discontinued!  

#### Website
[Project website](https://pott.app)  
[Barista URL](https://barista.pott.app/barista/info)  